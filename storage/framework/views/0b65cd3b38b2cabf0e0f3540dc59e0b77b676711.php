
<?php $__env->startSection('conteudo'); ?>
        <h1 class="title"><?php echo e($titulo); ?></h1>
        <a class="subtitle" href="<?php echo e(route('client.create')); ?>">Criar</a>
        <hr>
            <?php if(count($clients)>0): ?>
                <ul >
                    <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li>
                            <?php echo e($c['id']); ?>: <?php echo e($c['name']); ?>

                            <a href="<?php echo e(route('client.edit', $c['id'])); ?>">Editar</a> |
                            <a href="<?php echo e(route('client.show', $c['id'])); ?>">Info</a> | 
                            <form action="<?php echo e(route('client.destroy', $c['id'] )); ?>" method="POST">
                                <?php echo csrf_field(); ?>
                                <?php echo method_field('DELETE'); ?>
                                <input class="button is-small" type="submit" value="Delete">
                            </form>
                        </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
                <hr>
                

                <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
    <?php if(empty($clients)): ?>
            <h4>Não existem clientes cadastrados</h4>
    <?php endif; ?>
<?php $__env->stopSection(); ?>




<?php echo $__env->make('templates.principal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\test\resources\views/clients/index.blade.php ENDPATH**/ ?>