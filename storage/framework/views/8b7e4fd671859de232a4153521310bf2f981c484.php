
<?php $__env->startSection('conteudo'); ?>
    <div>
        <ul class="options">
            <li><a class="<?php echo e(request()->routeIs('opcoes',1) ? 'danger selected' : ''); ?>" href="<?php echo e(route('opcoes',1)); ?>">warning</a></li>
            <li><a class="<?php echo e(request()->routeIs('opcoes',2)  ? 'info selected' : ''); ?>"    href="<?php echo e(route('opcoes',2)); ?>">info</a></li>
            <li><a class="<?php echo e(request()->routeIs('opcoes',3)  ? 'success selected' : ''); ?>" href="<?php echo e(route('opcoes',3)); ?>">sucess</a></li>
            <li><a class="<?php echo e(request()->routeIs('opcoes',4)  ? 'error selected' : ''); ?>"   href="<?php echo e(route('opcoes',4)); ?>">error</a></li>
        </ul>
    </div>

<?php if(isset($opcao)): ?>

    <?php switch($opcao):

    case (1): ?>
    <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.alert','data' => ['titulo' => 'Alert!','tipo' => 'info']]); ?>
<?php $component->withName('alert'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['titulo' => 'Alert!','tipo' => 'info']); ?>
        <p><strong>Lorem ipsum! </strong></p>
     <?php echo $__env->renderComponent(); ?>
<?php endif; ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
        <?php break; ?>

    <?php case (2): ?>
        <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.alert','data' => ['titulo' => 'Alert!','tipo' => 'sucess']]); ?>
<?php $component->withName('alert'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['titulo' => 'Alert!','tipo' => 'sucess']); ?>
            <p><strong>Lorem ipsum! </strong></p>
         <?php echo $__env->renderComponent(); ?>
<?php endif; ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
            <?php break; ?>

    <?php case (3): ?>
    <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.alert','data' => ['titulo' => 'Alert!','tipo' => 'primary']]); ?>
<?php $component->withName('alert'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['titulo' => 'Alert!','tipo' => 'primary']); ?>
        <p><strong>Lorem ipsum! </strong></p>
     <?php echo $__env->renderComponent(); ?>
<?php endif; ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
    <?php break; ?>

    <?php case (4): ?>
    <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.alert','data' => ['titulo' => 'Alert!','tipo' => 'link']]); ?>
<?php $component->withName('alert'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['titulo' => 'Alert!','tipo' => 'link']); ?>
        <p><strong>Lorem ipsum! </strong></p>
     <?php echo $__env->renderComponent(); ?>
<?php endif; ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
    <?php break; ?>
    <?php default: ?>

    <?php endswitch; ?>
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('templates.principal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\test\resources\views/other/opcoes.blade.php ENDPATH**/ ?>