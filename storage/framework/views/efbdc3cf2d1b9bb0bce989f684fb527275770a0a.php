<div class="notification is-<?php echo e($tipo); ?> is-small">
    <div class=""><h5 class="title is-5"><?php echo e($titulo); ?></h5></div>
    <div class="msg"><?php echo e($slot); ?></div>
</div>

<?php /**PATH C:\xampp\htdocs\test\resources\views/components/alert.blade.php ENDPATH**/ ?>