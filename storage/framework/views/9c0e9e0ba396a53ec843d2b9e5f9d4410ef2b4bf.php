<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <link rel="stylesheet" href="<?php echo e(asset('css/style.css')); ?>">
    <title>PHP Study</title>
</head>
<body> 
    <div class="flex-container">
        <div class="flex-item-left">
            <div class="menu">
                <p class="menu-label">
                    Menu
                </p>
                <ul class="menu-list">
                    <li><a class="<?php echo e(request()->routeIs('client.*') ? 'is-active' : ''); ?>" href="<?php echo e(route('client.index')); ?>">Clients</a></li>
                    <li><a class="<?php echo e(request()->routeIs('dashboard') ? 'is-active' : ''); ?>" href="<?php echo e(route('dashboard')); ?>">Dashboard</a></li>
                    <li><a class="<?php echo e(request()->routeIs('customers') ? 'is-active' : ''); ?>"  href="<?php echo e(route('customers')); ?>">Customers</a></li>
                    <li><a class="<?php echo e(request()->routeIs('opcoes') ? 'is-active' : ''); ?>"  href="<?php echo e(route('opcoes')); ?>">Opções</a></li>
                </ul>
            </div>       
        </div>
        <div class="flex-item-right">
            <div class="">
                <?php echo $__env->yieldContent('conteudo'); ?>
            </div>
        </div>
      </div>
            
</body>
</html><?php /**PATH C:\xampp\htdocs\test\resources\views/templates/principal.blade.php ENDPATH**/ ?>