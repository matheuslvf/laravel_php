
<?php $__env->startSection('conteudo'); ?>
    <h3 class="title">New client:</h3>
    <hr>
    <form action="<?php echo e(route('client.update', $client['id'] )); ?>" method="POST">
        <?php echo csrf_field(); ?>
        <?php echo method_field('PUT'); ?>
        <input type="text" name="name" value="<?php echo e($client['name']); ?>">
        <input type="submit" value="Salvar">
    </form>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('templates.principal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\test\resources\views/clients/edit.blade.php ENDPATH**/ ?>