<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DrugsController extends Controller
{

    private $clients = [
        ['id'=> 1, 'name'=> 'Matheus'],
        ['id'=> 2, 'name'=> 'Davi'],
        ['id'=> 3, 'name'=> 'Maria'],
        ['id'=> 4, 'name'=> 'Guilherme']
    ];

    public function __construct (){
        $clients = session('clients');
        if (!isset($clients)) 
            session(['clients' => $this->clients]);
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = session('clients');
        return view ('clients.index', ['clients'=>$clients,'titulo'=>'All Clients']); //array associativa

        //with
        // return view ('clients.index')
        //                     ->with('clients', $clients)                 
        //                     ->with('titulo', 'Clients');

                            //compact
        //return view ('clients.index', compact(['client']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $clients = session('clients');
        $id = array_key_last($clients) + 1;
        $name = $request -> name;
        $data = ['id'=> $id, 'name'=> $name];
        $clients[] = $data;
        session(['clients' => $clients]);
        return redirect()->route('client.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $clientes = session('clients');
        $index = $this->getIndex($id, $clientes);
        $client = $clientes [$index];
        return view('clients.info', compact(['client']));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clientes = session('clients');
        $index = $this->getIndex($id, $clientes);
        $client = $clientes [$index];
        return view('clients.edit', compact(['client']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $clientes = session('clients');
        $index = $this->getIndex($id, $clientes);
        $clientes[$index]['name'] = $request->name;
        session(['clients' => $clientes]);
        return redirect()->route('client.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $clients = session('clients');
        $index = $this->getIndex($id, $clients);
        array_splice($clients, $index, 1);
        session(['clients' => $clients]);
        return redirect()->route('client.index');
    }

    private function getIndex($id, $clients){
        $ids = array_column($clients, 'id');
        $index = array_search($id,$ids);
        return $index;
    }
    

}
