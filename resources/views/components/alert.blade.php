<div class="notification is-{{$tipo}} is-small">
    <div class=""><h5 class="title is-5">{{$titulo}}</h5></div>
    <div class="msg">{{$slot}}</div>
</div>

