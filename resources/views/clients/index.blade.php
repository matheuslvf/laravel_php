@extends('templates.principal')
@section('conteudo')
        <h1 class="title">{{$titulo}}</h1>
        <a class="subtitle" href="{{route('client.create')}}">Criar</a>
        <hr>
            @if (count($clients)>0)
                <ul >
                    @foreach ($clients as $c)
                        <li>
                            {{$c['id']}}: {{$c['name']}}
                            <a href="{{route('client.edit', $c['id'])}}">Editar</a> |
                            <a href="{{route('client.show', $c['id'])}}">Info</a> | 
                            <form action="{{route('client.destroy', $c['id'] )}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input class="button is-small" type="submit" value="Delete">
                            </form>
                        </li>
                    @endforeach
                </ul>
                <hr>
                

                @foreach ($clients as $c)
                    
                @endforeach
            @endif
    @empty($clients)
            <h4>Não existem clientes cadastrados</h4>
    @endempty
@endsection



