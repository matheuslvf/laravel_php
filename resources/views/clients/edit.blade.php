@extends('templates.principal')
@section('conteudo')
    <h3 class="title">New client:</h3>
    <hr>
    <form action="{{route('client.update', $client['id'] )}}" method="POST">
        @csrf
        @method('PUT')
        <input type="text" name="name" value="{{$client['name']}}">
        <input type="submit" value="Salvar">
    </form>
@endsection