@extends('templates.principal')
@section('conteudo')
    <div>
        <ul class="options">
            <li><a class="{{request()->routeIs('opcoes',1) ? 'danger selected' : ''}}" href="{{route('opcoes',1)}}">warning</a></li>
            <li><a class="{{request()->routeIs('opcoes',2)  ? 'info selected' : ''}}"    href="{{route('opcoes',2)}}">info</a></li>
            <li><a class="{{request()->routeIs('opcoes',3)  ? 'success selected' : ''}}" href="{{route('opcoes',3)}}">sucess</a></li>
            <li><a class="{{request()->routeIs('opcoes',4)  ? 'error selected' : ''}}"   href="{{route('opcoes',4)}}">error</a></li>
        </ul>
    </div>

@if (isset($opcao))

    @switch($opcao)

    @case(1)
    <x-alert titulo="Alert!" tipo="info">
        <p><strong>Lorem ipsum! </strong></p>
    </x-alert>
        @break

    @case(2)
        <x-alert titulo="Alert!" tipo="sucess">
            <p><strong>Lorem ipsum! </strong></p>
        </x-alert>
            @break

    @case(3)
    <x-alert titulo="Alert!" tipo="primary">
        <p><strong>Lorem ipsum! </strong></p>
    </x-alert>
    @break

    @case(4)
    <x-alert titulo="Alert!" tipo="link">
        <p><strong>Lorem ipsum! </strong></p>
    </x-alert>
    @break
    @default

    @endswitch
@endif
@endsection