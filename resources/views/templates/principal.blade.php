<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <title>PHP Study</title>
</head>
<body> 
    <div class="flex-container">
        <div class="flex-item-left">
            <div class="menu">
                <p class="menu-label">
                    Menu
                </p>
                <ul class="menu-list">
                    <li><a class="{{request()->routeIs('client.*') ? 'is-active' : ''}}" href="{{route('client.index')}}">Clients</a></li>
                    <li><a class="{{request()->routeIs('dashboard') ? 'is-active' : ''}}" href="{{route('dashboard')}}">Dashboard</a></li>
                    <li><a class="{{request()->routeIs('customers') ? 'is-active' : ''}}"  href="{{route('customers')}}">Customers</a></li>
                    <li><a class="{{request()->routeIs('opcoes') ? 'is-active' : ''}}"  href="{{route('opcoes')}}">Opções</a></li>
                </ul>
            </div>       
        </div>
        <div class="flex-item-right">
            <div class="">
                @yield('conteudo')
            </div>
        </div>
      </div>
            
</body>
</html>