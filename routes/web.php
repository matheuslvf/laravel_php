<?php

use App\Http\Controllers\drugsController;
use Illuminate\Support\Facades\Route;
use Illuminate\http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('client', DrugsController::class);

Route::get('dashboard', function(){
    return view('other.dashboard');
})->name('dashboard');

Route::get('customers', function(){
    return view('other.customers');
})->name('customers');

Route::get('opcoes/{opcao?}', function($opcao=null){
    return view('other.opcoes', compact(['opcao']));
})->name('opcoes');



// Route::get('/hello/{name}', function ($name) {
//     return 'Olá! Isso é Laravel! Bem vindo ' . $name . '!';
// });

// Route::get('/hello/{name?}', function ($name=null) {
//     if(isset($name))
//         return 'Olá! Isso é Laravel! Bem vindo ' . $name . '!';
//     else
//         return 'Olá! Você não digitou seu nome!';
// });

// Route::get('/regras/{name}/{n}', function ($name, $n) {
//    for ($i=0;$i<$n;$i++)
//     echo  "$name! <br>";
// })-> where('name','[A-Za-z]+')->where('n','[0-9]+');

// Route::post('/requisicoes', function (Request $request) {
//     return 'Hello POST';
// });
